import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import axios from 'axios'
import Filters from './components/filters'
import WeekMenuPage from './components/weekMenu'
import ShoppingList from './components/shoppingList'


export default function App() {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name='Week menu'
          component={WeekMenuPage}
          options={{ title: 'Hello', headerMode: 'none' }}
        />
        <Stack.Screen
          name='Set filters'
          component={Filters}
          options={{ title: 'Hello', headerMode: 'none' }}
        />
         <Stack.Screen
          name='Shopping list'
          component={ShoppingList}
          options={{ title: 'Hello', headerMode: 'none' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );

}

