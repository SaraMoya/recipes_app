import React, { useState, useEffect } from 'react';
import { CheckBox, Icon, Overlay, Button } from 'react-native-elements'
import Constants from 'expo-constants';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    AsyncStorage,
    Alert,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import { RFPercentage } from "react-native-responsive-fontsize";
import Drawer from 'react-native-drawer'
import Menu from './menu'

export default function Filters(props) {
    // balanced
    // high-protein
    // low-fat
    // low-carb
    // vegan
    // vegetarian
    // sugar-conscious
    // peanut-free
    // tree-nut-free
    // alcohol-free
    const [availableFilters, setAvailableFilters] = useState([{ filter: 'balanced', checked: false }, { filter: 'high-protein', checked: false }, { filter: 'low-fat', checked: false }, { filter: 'low-carb', checked: false }, { filter: 'vegan', checked: false }, { filter: 'vegetarian', checked: false }, { filter: 'sugar-conscious', checked: false }, { filter: 'peanut-free', checked: false }, { filter: 'tree-nut-free', checked: false }, { filter: 'alcohol-free', checked: false }])
    const [filters, setFilters] = useState([])
    const [showAvalaibleFilters, SetShowAvalaibleFilters] = useState(false)
    const [drawer, setDrawer] = useState(null)
    const closeDrawer = () => setDrawer(false)
    const renderMenu = () => <Menu closeDrawer={closeDrawer} />

    useEffect(() => {
        getFilters()
    }, []);

    useEffect(() => {
        getFilters()
    }, [showAvalaibleFilters]);

    const getFilters = async () => {
        let temp = []
        let storage = await AsyncStorage.getItem('availableFilters')
        let availableFilters = JSON.parse(storage)
        setAvailableFilters(availableFilters)
        availableFilters.map(filter => filter.checked === true ? temp.push(filter.filter) : null)
        await AsyncStorage.setItem('filters', JSON.stringify(temp))
        setFilters(temp)
    }

    const removeFilter = async (i) => {
        let storage = await AsyncStorage.getItem('availableFilters')
        let storageAF = JSON.parse(storage)
        storageAF.map(filter => filter.filter === filters[i] ? filter.checked = false : null)
        setAvailableFilters([...storageAF])
        await AsyncStorage.setItem('availableFilters', JSON.stringify(storageAF))
        return getFilters()
    }

    const resetFilters = async () => {
        let storage = await AsyncStorage.getItem('availableFilters')
        let storageAF = JSON.parse(storage)
        storageAF.map(filter => filter.checked = false)
        setAvailableFilters([...storageAF])
        await AsyncStorage.setItem('availableFilters', JSON.stringify(storageAF))
        return getFilters()
    }

    const setFiltersChecked = async (i) => {
        let storage = await AsyncStorage.getItem('availableFilters')
        let storageAF = JSON.parse(storage)
        storageAF[i].checked = !storageAF[i].checked
        setAvailableFilters([...storageAF])
        await AsyncStorage.setItem('availableFilters', JSON.stringify([...storageAF]))
        return getFilters()
    }

    const renderAvailableFilters = () => {
        return availableFilters.map((filter, i) => {
            return <CheckBox
                title={filter.filter}
                iconType='feather'
                iconLeft
                checkedIcon='check-square'
                uncheckedIcon='square'
                checked={filter.checked}
                textStyle={{ color: 'white', fontSize: RFPercentage(3) }}
                containerStyle={{ backgroundColor: "rgba(255, 255, 255, 0.2)", borderWidth: 0, }}
                onPress={() => {
                    setFiltersChecked(i)
                }}
            />
        })
    }

    return (
        <Drawer
            open={drawer}
            content={renderMenu()}
            type="overlay"
            tapToClose={true}
            styles={{ backgroundColor: 'green' }}
            openDrawerOffset={0}
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            onClose={() => {
                setDrawer(false);
            }}
            panOpenMask={0.8}
            captureGestures="open"
            acceptPan={true}>
            <ImageBackground source={require('../assets/bg2.png')} style={styles.container}>
                <View style={styles.menu_bar}>
                    <Icon
                        name='menu'
                        type='feather'
                        color='white'
                        size={40}
                        underlayColor={'transparent'}
                        onPress={() => setDrawer(true)}
                    />
                </View>
                <View style={styles.filters_container}>
                    <View style={styles.filters_bar}>
                        <Text style={styles.title}>Set filters</Text>
                        {filters.length > 0 ? <Button
                            buttonStyle={{ borderColor: 'white', borderWidth: 1, backgroundColor: 'rgba(000, 000, 000, 0.50);' }}
                            titleStyle={{ color: 'white', fontSize: RFPercentage(2.5) }}
                            title="Reset filters"
                            type="outline"
                            onPress={() => Alert.alert(
                                'All the filters will be removed, are you sure?',
                                '',
                                [{ text: 'yes', onPress: () => resetFilters() }, { text: 'cancel', onPress: () => console.log('remove filter cancelled') }]
                            )}
                        /> : null}
                    </View>

                    <View style={styles.filter_container}>
                        {filters.length > 0 ? filters.map((filter, i) => <TouchableOpacity style={styles.filter}>
                            <Text style={styles.filter_text}>{filter}</Text>
                            <Icon
                                name='x'
                                type='feather'
                                color='white'
                                size={20}
                                underlayColor={'transparent'}
                                onPress={() => Alert.alert(
                                    `The filter "${filter}" will be removed, are you sure?`,
                                    '',
                                    [{ text: 'yes', onPress: () => removeFilter(i) }, { text: 'cancel', onPress: () => console.log('remove filter cancelled') }]
                                )}
                            />
                        </TouchableOpacity>) : <Text style={styles.filter_text}>No filters set, add a new one</Text>}
                        <View style={styles.add}>
                            <Icon
                                name='plus'
                                type='feather'
                                color='white'
                                size={50}
                                underlayColor={'transparent'}
                                onPress={() => SetShowAvalaibleFilters(true)}
                            />
                        </View>


                        <Overlay
                            isVisible={showAvalaibleFilters}
                            windowBackgroundColor="rgba(000, 000, 000, 0.8)"
                            overlayBackgroundColor="rgba(000, 000, 000, 0.5)"
                            width="100%"
                            height="95%"
                        >
                            <View style={styles.filters_picker}>
                                <ScrollView style={styles.filters_boxes}>
                                    {renderAvailableFilters()}
                                </ScrollView>
                                <View style={styles.overlay_button}>
                                    <Button
                                        buttonStyle={{ borderColor: 'white', borderWidth: 3, backgroundColor: 'rgba(000, 000, 000, 0.50);', width: 140 }}
                                        titleStyle={{ color: 'white', fontSize: RFPercentage(3) }}
                                        title="Close"
                                        type="outline"
                                        onPress={() => SetShowAvalaibleFilters(false)}
                                    />
                                </View>
                            </View>
                        </Overlay >
                    </View>
                </View>
            </ImageBackground>
        </Drawer>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
    },
    menu_bar: {
        marginTop: '2%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: '4%',
        paddingRight: '4%'
    },
    filters_container: {
        padding: 20,
        marginTop: '5%'
    },
    filters_bar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        fontSize: RFPercentage(3),
    },
    filter_container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 20,
        alignItems: 'center'
    },
    filter: {
        backgroundColor: 'rgba(255, 255, 255, 0.20);',
        borderWidth: 2,
        borderColor: 'white',
        borderRadius: 2,
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 12,
        marginBottom: 12,
        padding: 5,
        paddingRight: 10,
        paddingLeft: 10
    },
    filter_text: {
        color: 'white',
        fontSize: RFPercentage(3),
        paddingRight: 10
    },
    add: {
        flexDirection: 'row',
        textAlign: 'left'
    },
    filters_picker: {
        paddingRight: 40,
        paddingLeft: 40,
    },
    filters_boxes: {
        height: '80%'
    },
    overlay_button: {
        flexDirection: 'row',
        marginTop: 30,
        justifyContent: 'center'
    }
});
