import React, { useState, useEffect } from 'react';
import { CheckBox, Icon, Overlay, Button } from 'react-native-elements'
import Constants from 'expo-constants';
import {
    StyleSheet,
    View,
    ScrollView,
    AsyncStorage,
    ImageBackground,
} from 'react-native';
import { RFPercentage } from "react-native-responsive-fontsize";
import Drawer from 'react-native-drawer'
import Menu from './menu'

export default function ShoppingList () {
    const [shoppingList, setShoppingList] = useState([{ item: 'potatos', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }, { item: 'grapes', checked: false }])
    const [drawer, setDrawer] = useState(null)
    const closeDrawer = () => setDrawer(false)
    const renderMenu = () => <Menu closeDrawer={closeDrawer} />

    // const getList = () => {
    //     let temp = []
    //     let storage = await AsyncStorage.getItem('recipes')
    //     let recipes = JSON.parse(storage)
    //     recipes.map(recipe => recipe.ingredients.map(ingredient =>  temp.push({item: ingredient, checked: false, amount: 1})))
    //     await AsyncStorage.setItem('shoppingList', JSON.stringify(temp))
    //     setShoppingList(temp)
    // }

    const renderList = () => {
        return shoppingList.map((item, i) => {
            return <CheckBox
                title={item.item}
                iconType='feather'
                iconLeft
                checkedIcon='check-square'
                uncheckedIcon='square'
                checked={item.checked}
                textStyle={{ color: 'white', fontSize: RFPercentage(3) }}
                containerStyle={{ backgroundColor: "rgba(255, 255, 255, 0.2)", borderWidth: 0, }}
                onPress={() => {
                    markedItem(i)
                }}
            />
        })
    }

    const markedItem = async () => {
        let storage = await AsyncStorage.getItem('shoppingList')
        let storageAF = JSON.parse(storage)
        storageAF[i].checked = !storageAF[i].checked
        setShoppingList([...storageAF])
        await AsyncStorage.setItem('ShoppingList', JSON.stringify([...storageAF]))
    }

    return (
        <Drawer
            open={drawer}
            content={renderMenu()}
            type="overlay"
            tapToClose={true}
            styles={{ backgroundColor: 'green' }}
            openDrawerOffset={0}
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            onClose={() => {
                setDrawer(false);
            }}
            panOpenMask={0.8}
            captureGestures="open"
            acceptPan={true}>
            <ImageBackground source={require('../assets/bg2.png')} style={styles.container}>
                <View style={styles.menu_bar}>
                    <Icon
                        name='menu'
                        type='feather'
                        color='white'
                        size={40}
                        underlayColor={'transparent'}
                        onPress={() => setDrawer(true)}
                    />
                </View>
                <View style={styles.shopping_container}>
                    <ScrollView  style={styles.shopping_list}>
                        {shoppingList.length > 0 ? renderList() : null}
                    </ScrollView>
                </View>
            </ImageBackground>
        </Drawer>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
    },
    menu_bar: {
        marginTop: '2%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: '4%',
        paddingRight: '4%'
    },
    shopping_container: {
        padding: 20,
        marginTop: '5%'
    },
    shopping_list: {
        height: '98%'
    }
});
