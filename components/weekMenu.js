import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    AsyncStorage,
    Alert,
    ImageBackground
} from 'react-native';
import axios from 'axios'
import { Icon, Button } from 'react-native-elements'
import Constants from 'expo-constants';
import { Linking } from 'expo'
import { RFPercentage } from "react-native-responsive-fontsize";
import Drawer from 'react-native-drawer'
import Menu from './menu'


export default function WeekMenuPage() {
    const [recipes, setRecipes] = useState([
        { recipe: { label: 'receta 1', url: '#' } },
        { recipe: { label: 'receta 2', url: '#' } },
        { recipe: { label: 'receta 3', url: '#' } },
        { recipe: { label: 'receta 4', url: '#' } },
        { recipe: { label: 'receta 5', url: '#' } },
        { recipe: { label: 'receta 6', url: '#' } },
        { recipe: { label: 'receta 7', url: '#' } },
        { recipe: { label: 'receta 8', url: '#' } },
        { recipe: { label: 'receta 9', url: '#' } }])
    const [days] = useState(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'])
    const [weekMenu, setWeekMenu] = useState([])
    const [drawer, setDrawer] = useState(null)
    const closeDrawer = () => setDrawer(false)

    // useEffect(() => {
    //   (async () => {
    //     const api_key = "a8d9660ab392986fec73a9b8b3bafb5d";
    //     const app_id = "ca6a0f8a";
    //     try {
    //       const response = await axios.get(
    //         `https://api.edamam.com/search?q=chicken&app_id=${app_id}&app_key=${api_key}&from=0&to=15&calories=591-722&health=alcohol-free`
    //       );
    //       setRecipes([...response.data.hits]);
    //       console.log('recipes', response.data.hits)
    //     } catch (error) {
    //       console.log(error);
    //     }          
    //   })();
    // }, []);


    useEffect(() => {
        getStorage()
    }, []);

    const getIndexes = () => {
        let max = recipes.length - 1
        let temp = []
        days.map(day => {
            let index = Math.round(Math.random() * max)
            let index2 = Math.round(Math.random() * max)
            return index !== index2 ? temp.push(index, index2) : index = Math.round(Math.random() * max)
        })
        setStorage(temp)
    }

    const setStorage = async (indexes) => {
        let tempStorage = await AsyncStorage.getItem('weekMenu') || []
        let tempWeekMenu = typeof tempStorage === String ? JSON.parse(tempStorage) : tempStorage
        indexes.map(index => tempWeekMenu.push(recipes[index]))
        setWeekMenu([...tempWeekMenu])
        await AsyncStorage.setItem('weekMenu', JSON.stringify([...tempWeekMenu]))
    }

    const getStorage = async () => {
        let current = await AsyncStorage.getItem('weekMenu')
        !current ? getIndexes() : setWeekMenu([...JSON.parse(current)])
    }

    const reloadDay = async (i) => {
        let max = recipes.length - 1
        let tempWeekMenu = weekMenu
        let newIndex = Math.round(Math.random() * max)
        return newIndex !== i ? (
            tempWeekMenu[i] = tempWeekMenu[newIndex],
            await AsyncStorage.removeItem('weekMenu'),
            await AsyncStorage.setItem('weekMenu', JSON.stringify([...tempWeekMenu])),
            getStorage()
        ) : newIndex = Math.round(Math.random() * max)
    }

    const refreshMenu = async () => {
        await AsyncStorage.removeItem('weekMenu')
        return getIndexes()
    }

    const renderDay = () => {
        return days.map((day, i) => <View key={i} style={styles.day_container}>
            <View style={styles.day_title}>
                <Text style={[styles.text, styles.title]}>{day}</Text>
                <Icon
                    name='refresh-ccw'
                    type='feather'
                    color='white'
                    size={25}
                    onPress={() => Alert.alert(
                        `${day} menu will refresh, are you sure?`,
                        '',
                        [{ text: 'yes', onPress: () => reloadDay(i) }, { text: 'cancel', onPress: () => console.log('reload day cancelled') }]
                    )}
                />
            </View>
            <Text style={[styles.text, styles.meal]} >Lunch: </Text>
            <Text style={styles.text}>{weekMenu.length > 0 ? weekMenu[i].recipe.label : 'Loading...'}</Text>
            <View style={styles.link_container}>
                <Text style={styles.link} onPress={() => Linking.openURL(weekMenu[i].recipe.url)}>Go to recipe</Text>
                <Icon
                    name='chevrons-right'
                    type='feather'
                    color='#8cba51'
                    underlayColor={'transparent'}
                    size={30}
                />
            </View>
            <Text style={[styles.text, styles.meal]}>Dinner: </Text>
            <Text style={styles.text}>{weekMenu.length > 0 ? weekMenu.reverse()[i].recipe.label : 'Loading...'}</Text>
            <View style={styles.link_container}>
                <Text style={styles.link} onPress={() => Linking.openURL(weekMenu.reverse()[i].recipe.url)}>Go to recipe</Text>
                <Icon
                    name='chevrons-right'
                    type='feather'
                    color='#8cba51'
                    underlayColor={'transparent'}
                    size={30}
                />
            </View>
        </View>)
    }

    const renderMenu = () => <Menu closeDrawer={closeDrawer} />

    return (
        <Drawer
            open={drawer}
            content={renderMenu()}
            type="overlay"
            tapToClose={true}
            styles={{ backgroundColor: 'green' }}
            openDrawerOffset={0}
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            onClose={() => {
                setDrawer(false);
            }}
            panOpenMask={0.8}
            captureGestures="open"
            acceptPan={true}>
            <ImageBackground source={require('../assets/bg.png')} style={styles.container}>
                <View style={styles.menu_bar}>
                    <Icon
                        name='menu'
                        type='feather'
                        color='white'
                        size={40}
                        underlayColor={'transparent'}
                        onPress={() => setDrawer(true)}
                    />
                    <Button
                        buttonStyle={{ borderColor: 'white', borderWidth: 3, backgroundColor: 'rgba(000, 000, 000, 0.50);'}}
                        titleStyle={{ color: 'white', fontSize: RFPercentage(3) }}
                        title="Refresh menu"
                        type="outline"
                        onPress={() => Alert.alert(
                            'week menu will refresh, are you sure?',
                            '',
                            [{ text: 'yes', onPress: () => refreshMenu() }, { text: 'cancel', onPress: () => console.log('refresh cancelled') }]
                        )}
                    />
                </View>
                <ScrollView style={styles.week_menu}>
                    {weekMenu.length > 0 ? renderDay()
                        : <Icon
                            name='loader'
                            type='feather'
                            color='white'
                            size={120}
                        />}
                </ScrollView>
            </ImageBackground>
        </Drawer>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
    },
    menu_bar: {
        marginTop: '2%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: '4%',
        paddingRight: '4%'
    },
    week_menu: {
        margin: 20
    },
    day_container: {
        backgroundColor: 'rgba(255, 255, 255, 0.16);',
        padding: 20,
        width: '100%',
        marginTop: 20,
    },
    day_title: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    title: {
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    text: {
        color: 'white',
        fontSize: RFPercentage(3),
        letterSpacing: 1,
    },
    link_container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    link: {
        color: '#8cba51',
        fontSize: RFPercentage(2.9),
        letterSpacing: 1,
        fontWeight: 'bold',
        paddingRight: 10,
    },
    meal: {
        fontSize: RFPercentage(2.5),
        paddingTop: 15,
    },
    button: {
        color: 'white',
        fontSize: RFPercentage(3),
    }
});
