import 'react-native-gesture-handler';
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native'
import { Icon } from 'react-native-elements'
import { RFPercentage } from "react-native-responsive-fontsize";
import Constants from 'expo-constants';
import { useNavigation } from '@react-navigation/native';

export default function Menu(props) {
    const navigation = useNavigation();
    console.log('navigation', navigation)
    return (
        <View style={styles.menu_container}>
            <TouchableOpacity style={styles.menu_icon}>
                <Icon
                    name='menu'
                    type='feather'
                    color='white'
                    size={40}
                    underlayColor={'transparent'}
                    onPress={() => props.closeDrawer()}
                />
            </TouchableOpacity>
            <TouchableOpacity style={styles.link}  onPress={() => {
                  navigation.navigate('Week menu')
                props.closeDrawer()
              
            } }>
                <Icon
                    name='layout'
                    type='feather'
                    color='white'
                    size={25}
                /><Text style={styles.text}>Week menu</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.link}   onPress={() => {
                navigation.navigate('Set filters')
                props.closeDrawer()
                }}>
                <Icon
                    name='filter'
                    type='feather'
                    color='white'
                    size={25}
                /><Text style={styles.text}>Set filters</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.link}  onPress={() => {
                navigation.navigate('Shopping list')
                props.closeDrawer()}}>
                <Icon
                    name='shopping-cart'
                    type='feather'
                    color='white'
                    size={25}
                /><Text style={styles.text}>Shopping list</Text>
            </TouchableOpacity>
        </View>

    )
}


const styles = StyleSheet.create({
    menu_container: {
        flex: 1,
        backgroundColor: "#222328",
        padding: 10,
        color: 'white',
        fontSize: RFPercentage(3),
        marginTop: Constants.statusBarHeight,
    },
    text: {
        color: 'white',
        fontSize: RFPercentage(3),
        paddingLeft: 10
    },
    link: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(255, 255, 255, 0.16)'
    },
    menu_icon : {
        flexDirection: 'row'
    }
})

